<!-- GITLAB-->
## Olá! Eu sou o Lucas Passos.


- :computer:  Computer Science
- :seedling:  Studying Typescript
- :zap:  Internship at Supremo Tribunal Federal 

##
<div>
<!-- acesso github-->
 Para projetos pessoais de desenvolvimento independente:

 # <a href="https://github.com/alendadepassos" target="_blank"><img src="https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white" target="_blank"></a> 

</div>

##

<!-- Most Used Languages-->
<div align="center">
  <a href="https://github.com/alendadepassos">
  
  <img height="150em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=alendadepassos&layout=compact&langs_count=7&theme=radical"/>
  <img height="150em" src="https://github-readme-stats.vercel.app/api?username=alendadepassos&&count_private=true&show_icons=true&theme=radical"/>
</div>

##  
<!-- known languages -->
<div style="display: inline_block"><br>
  <div align="center">
  <img align="center" alt="Lucas-Js" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
  <img align="center" alt="Lucas-Ts" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-plain.svg">
  <img align="center" alt="Lucas-Java" height="40" width="50" src="https://raw.githubusercontent.com/jmnote/z-icons/master/svg/java.svg">
  <img align="center" alt="Lucas-HTML" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
  <img align="center" alt="Lucas-CSS " height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
  <img align="center" alt="Lucas-Python " src="https://img.shields.io/badge/Python-14354C?style=for-the-badge&logo=python&logoColor=white">
  <img align="center" alt="Lucas-Angular " src="https://img.shields.io/badge/angular-%23DD0031.svg?style=for-the-badge&logo=angular&logoColor=white">
</div>
  </div>
 
##
   <!-- Contact -->
  <div> 
    <div align="center">
  <a href="https://www.linkedin.com/in/lucaspassos9/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>
  <a HREF="mailto:lucaspassos9@gmail.com"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a> 
  <a href="https://wa.me/5561992977915?text=Bom%20Dia!%20Vim%20pelo%20seu%20GitHub/GitLab" target="_blank"><img src="https://img.shields.io/badge/WhatsApp-25D366?style=for-the-badge&logo=whatsapp&logoColor=white" target="_blank"></a>
  <a href="https://instagram.com/a_lenda_de_passos?igshid=YmMyMTA2M2Y=" target="_blank"><img src="https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=white" target="_blank"></a>
  <a href="https://github.com/alendadepassos" target="_blank"><img src="https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white" target="_blank"></a> 
  <a href="https://gitlab.com/lucas.s.passos" target="_blank"><img src="https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white" target="_blank"></a> 
  <a href="https://steamcommunity.com/id/alendadepassos" target="_blank"><img src="https://img.shields.io/badge/steam-%23000000.svg?style=for-the-badge&logo=steam&logoColor=white" target="_blank"></a> 
</div>



